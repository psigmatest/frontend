# Dashboard

    Iniciar con las siguientes credenciales a la plataforma

    Correo Electronico
     psigmatest@corporation.com

    Contraseña
     test1234

# Tecnologias
    - Angular v11
    - Tailwindcss v3

## Instalacion y ejecución
    1) Clonar el repositorio
    
    2) realizar los siguientes comandos para su ejecución
        npm install
        ng s
        npm run tw

    3) Abrir el navegador con el puerto `http://localhost:4200/`

