// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api:{
    auth: 'http://127.0.0.1:8000/api/v1/auth',
    logout: 'http://127.0.0.1:8000/api/v1/logout',
    drivers: 'http://127.0.0.1:8000/api/v1/drivers',
    driverCreate: 'http://127.0.0.1:8000/api/v1/driver/create',
    driverUpdate: 'http://127.0.0.1:8000/api/v1/driver/update',
    driverDelete: 'http://127.0.0.1:8000/api/v1/driver/delete',
    vehicles: 'http://127.0.0.1:8000/api/v1/vehicles',
    vehicleCreate: 'http://127.0.0.1:8000/api/v1/vehicle/create',
    vehicleUpdate: 'http://127.0.0.1:8000/api/v1/vehicle/update',
    vehicleDelete: 'http://127.0.0.1:8000/api/v1/vehicle/delete',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
