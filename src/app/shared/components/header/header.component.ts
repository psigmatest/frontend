import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent{

  public isOpenDropdownUser:boolean = false;

  public openDropdownUser():void {
    this.isOpenDropdownUser = !this.isOpenDropdownUser;
  }

}
