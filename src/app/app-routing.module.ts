import { AuthGuard } from './core/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    pathMatch: 'full',
    redirectTo:'login'
  },
  {
    path:'login',
    loadChildren: ()=>import('./modules/auth/auth.module').then(m=>m.AuthModule)
  },
  {
    path:'home',
    canActivate:[AuthGuard],
    loadChildren: ()=>import('./modules/home/home.module').then(m=>m.HomeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
