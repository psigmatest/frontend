import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  public login(user:any){
    return this.http.post(environment.api.auth,user,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }

  public logout(){
    return this.http.post(environment.api.logout,'',{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+sessionStorage.getItem('_tkn')
      })
    })
  }

}
