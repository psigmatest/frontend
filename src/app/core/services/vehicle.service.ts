import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http:HttpClient){}

  public find(): Observable<any>{
    return this.http.get(environment.api.vehicles);
  }

  public create(vehicle:any): Observable<any>{
    return this.http.post(environment.api.vehicleCreate,vehicle,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }

  public update(vehicle:any): Observable<any>{
    return this.http.post(environment.api.vehicleUpdate,vehicle,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }
  
  public delete(vehicle:any): Observable<any>{
    return this.http.post(environment.api.vehicleDelete,vehicle,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }

}
