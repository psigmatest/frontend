import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor(private http:HttpClient){}

  public find(): Observable<any>{
    return this.http.get(environment.api.drivers);
  }

  public create(driver:any): Observable<any>{
    return this.http.post(environment.api.driverCreate,driver,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }
  
  public update(driver:any): Observable<any>{
    return this.http.post(environment.api.driverUpdate,driver,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }

  public delete(driver:any): Observable<any>{
    return this.http.post(environment.api.driverDelete,driver,{
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/json'
      })
    })
  }
}
