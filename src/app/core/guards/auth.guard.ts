import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(public _router: Router) { }

  canActivate(): boolean {
    let token = sessionStorage.getItem('_tkn');
    if (token) {
      return true;
    } else {
      this._router.navigate(['/login']);
      return false;
    }
  }
}
