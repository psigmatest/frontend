import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: [
    './auth.component.scss'
  ]
})
export class AuthComponent implements OnInit {

  public formGroup: FormGroup;
  public hasErrorsEmail: boolean = false;
  public hasErrorsPassword: boolean = false;
  public messageError:boolean;
  public isverifyingForm:boolean = false;

  constructor(
    private _auth:AuthService,
    public _formBuilder: FormBuilder,
    private _router:Router
  ){}

  ngOnInit(): void {
    this._auth.logout().subscribe(
      resp=>{
        sessionStorage.clear();
      }
    )
    //Contrucción de formulario reactivo para su validación
    this.formGroup = this._formBuilder.group({
      email: ['',[Validators.required, Validators.email]],
      password:['',Validators.required]
    });
  }


  public send():void {
    let emailErrors = this.formGroup.controls['email'].errors;
    let passwordErrors = this.formGroup.controls['password'].errors;

    if(emailErrors){
      this.hasErrorsEmail = true;
    }else{
      this.hasErrorsEmail = false;
    }

    if(passwordErrors){
      this.hasErrorsPassword = true;
    }else{
      this.hasErrorsPassword = false;
    }

    //Enviar peticion de autenticacion si pasa la validacion del form
    if(this.formGroup.valid){
      this.isverifyingForm = true;

      this._auth.login(this.formGroup.value).subscribe(
        (resp:any)=>{      
          this.isverifyingForm = false;
          sessionStorage.setItem('_tkn', JSON.stringify(resp.data.access_token));
          this._router.navigate(['home']).then(() => window.location.reload() );
        },
        (error:any)=>{
          this.isverifyingForm = false;
          this.messageError = error.error.data.message;
        }
      );
    }
  }

}
