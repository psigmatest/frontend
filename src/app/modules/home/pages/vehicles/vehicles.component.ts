import { DriverService } from './../../../../core/services/driver.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import * as XLSX from 'xlsx';
import { ModalService } from '../../services/modal.service';
import { VehicleService } from 'src/app/core/services/vehicle.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls:['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {

  public displayedColumns: string[] = ['marca','modelo','placa','drivers'];
  public dataSource:any;
  
  @ViewChild(MatPaginator) 
  paginator: MatPaginator;

  @ViewChild('table') 
  table: ElementRef;

  constructor(
    public _modal:ModalService,
    public _vehicles:VehicleService
  ){}

  ngOnInit(): void {
    this.renderVehicles();
  }

  freshTable(){
    this.renderVehicles();
  }
  
  renderVehicles(){
    this._vehicles.find().subscribe(
      (resp:any)=>{
        this.dataSource = new MatTableDataSource<any>(resp.data);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  exportAsExcel():void {
    let data = {...this.dataSource.data};
    
    const workSheet = XLSX.utils.json_to_sheet(Object.values(data));
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'vehiculos.xlsx');
  }

  openModalAdd(){
    this._modal.isOpenModalAdd.emit(true);
  }

  clickedRows(row:any){
    this._modal.dataClickRow.emit(row);
    this._modal.isOpenModalUpdate.emit(true);
  }

}
