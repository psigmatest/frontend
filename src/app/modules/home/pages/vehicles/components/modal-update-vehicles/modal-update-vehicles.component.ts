import { VehicleService } from 'src/app/core/services/vehicle.service';
import { Component, EventEmitter, Input, OnInit, Output, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from 'src/app/core/services/driver.service';
import { ModalService } from 'src/app/modules/home/services/modal.service';

@Component({
  selector: 'app-modal-update-vehicles',
  templateUrl: './modal-update-vehicles.component.html',
  styleUrls: ['./modal-update-vehicles.component.scss']
})
export class ModalUpdateVehiclesComponent implements OnInit {

  public isOpenModalUpdate:boolean = false;
  public formGroup: FormGroup;
  public isErrorsForm: boolean = false;
  public messageError:string;
  public isverifyingForm:boolean = false;
  public driversDataClick: any;
  public nameDrivers:any;
  public selectDriver:any;

  @Output()
  freshDataTable:EventEmitter<any> = new EventEmitter<any>();
  
  constructor(
    public _modal:ModalService,
    public _driver:DriverService,
    public _vehicle:VehicleService,
    public _formBuilder:FormBuilder
  ){}

  ngOnInit(): void {

    this._modal.dataClickRow.subscribe(
      (resp:any)=>{
        this.driversDataClick = resp;

        this.formGroup = this._formBuilder.group({
          uuid: [resp.uuid,[Validators.required]],
          marca: [resp.marca,[Validators.required]],
          modelo: [resp.modelo,[Validators.required]],
          placa: [resp.placa,[Validators.required]],
          fk_driver_id: [resp.drivers?resp.drivers.uuid:'null',[Validators.required]],
        });
      }
    )
    
    this._driver.find().subscribe(
      (resp:any)=>{
        this.nameDrivers = resp;
      }
    )

    this._modal.isOpenModalUpdate.subscribe(resp=>{
      this.isOpenModalUpdate = resp;
    });
  }

  closeModal(){
    this.isOpenModalUpdate = false;
  }

  send(){
    if(this.formGroup.value.fk_driver_id == 'null'){
      delete this.formGroup.value.fk_driver_id;
    }
    
    if(this.formGroup.valid){
      this._vehicle.update(this.formGroup.value).subscribe(
        (resp)=>{
          this.isverifyingForm = false;
          this.formGroup.reset();
          this.isOpenModalUpdate = false;
          this.freshDataTable.emit();
        },
        (error)=>{
          this.isErrorsForm = true;
          this.isverifyingForm = false;
          this.messageError = error.error.message;
        }
      )
    }else{
      this.isverifyingForm = false;
      this.isErrorsForm = true;
      this.messageError = 'Todos los campos son obligatorios';
    }
  }

  delete(){
    this._vehicle.delete(this.formGroup.value).subscribe(
      (resp)=>{
        this.isverifyingForm = false;
        this.formGroup.reset();
        this.isOpenModalUpdate = false;
        this.freshDataTable.emit();
      }
    );
  }
}
