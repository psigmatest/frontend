import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUpdateVehiclesComponent } from './modal-update-vehicles.component';

describe('ModalUpdateVehiclesComponent', () => {
  let component: ModalUpdateVehiclesComponent;
  let fixture: ComponentFixture<ModalUpdateVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalUpdateVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUpdateVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
