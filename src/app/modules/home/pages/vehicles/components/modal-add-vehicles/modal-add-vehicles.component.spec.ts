import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddVehiclesComponent } from './modal-add-vehicles.component';

describe('ModalAddVehiclesComponent', () => {
  let component: ModalAddVehiclesComponent;
  let fixture: ComponentFixture<ModalAddVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAddVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
