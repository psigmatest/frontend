import { VehicleService } from 'src/app/core/services/vehicle.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from './../../../../../../core/services/driver.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ModalService } from 'src/app/modules/home/services/modal.service';

@Component({
  selector: 'app-modal-add-vehicles',
  templateUrl: './modal-add-vehicles.component.html',
  styleUrls: ['./modal-add-vehicles.component.scss']
})
export class ModalAddVehiclesComponent implements OnInit {

  public isOpenModalAdd:boolean = false;
  public formGroup: FormGroup;
  public isErrorsForm: boolean = false;
  public messageError:string;
  public isverifyingForm:boolean = false;
  public nameDrivers:any;
  public selectDriver:string;

  @Output()
  freshDataTable:EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public _modal:ModalService,
    public _driver:DriverService,
    public _vehicle:VehicleService,
    public _formBuilder:FormBuilder
  ){

    this.formGroup = this._formBuilder.group({
      marca: ['',[Validators.required]],
      modelo: ['',[Validators.required]],
      placa: ['',[Validators.required]],
    });
  }
  
  ngOnInit(): void {
    this._driver.find().subscribe(
      (resp:any)=>{
        this.nameDrivers = resp;
      }
    )
    this._modal.isOpenModalAdd.subscribe(resp=>{
      this.isOpenModalAdd = resp;
    });
  }

  closeModal(){
    this.isOpenModalAdd = false;
  }

  send(){
    this.isverifyingForm = true;
    let data = {...this.formGroup.value};

    console.log(this.selectDriver);
    
    /* if(this.selectDriver != 'null'){
      data.fk_driver_id = this.selectDriver;
    } */

    if(this.formGroup.valid){
      this._vehicle.create(data).subscribe(
        (resp)=>{
          this.isverifyingForm = false;
          this.formGroup.reset();
          this.isOpenModalAdd = false;
          this.freshDataTable.emit();
        },
        (error)=>{
          this.isErrorsForm = true;
          this.isverifyingForm = false;
          this.messageError = error.errors;
        }
      )
    }else{
      this.isverifyingForm = false;
      this.isErrorsForm = true;
      this.messageError = 'Todos los campos son obligatorios';
    }
  }

}
