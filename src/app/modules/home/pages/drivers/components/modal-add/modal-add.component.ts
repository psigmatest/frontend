import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from './../../../../../../core/services/driver.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ModalService } from 'src/app/modules/home/services/modal.service';

@Component({
  selector: 'app-modal-add',
  templateUrl: './modal-add.component.html',
  styleUrls: ['./modal-add.component.scss']
})
export class ModalAddComponent implements OnInit{

  public isOpenModalAdd:boolean = false;
  public formGroup: FormGroup;
  public isErrorsForm: boolean = false;
  public messageError:string;
  public isverifyingForm:boolean = false;

  @Output()
  freshDataTable:EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public _modal:ModalService,
    public _driver:DriverService,
    public _formBuilder:FormBuilder
  ){

    this.formGroup = this._formBuilder.group({
      nombres: ['',[Validators.required]],
      apellidos: ['',[Validators.required]],
      identificacion: ['',[Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
      direccion: ['',[Validators.required]],
      telefono: ['',[Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
      ciudad_nacimiento: ['',[Validators.required]]
    });
  }
  
  ngOnInit(): void {
    this._modal.isOpenModalAdd.subscribe(resp=>{
      this.isOpenModalAdd = resp;
      this.messageError = undefined;
    });
  }

  closeModal(){
    this.isOpenModalAdd = false;
  }

  send(){
    this.isverifyingForm = true;

    if(this.formGroup.valid){
      this._driver.create(this.formGroup.value).subscribe(
        (resp)=>{
          this.isverifyingForm = false;
          this.formGroup.reset();
          this.isOpenModalAdd = false;
          this.freshDataTable.emit();
        },
        (error)=>{
          this.isErrorsForm = true;
          this.isverifyingForm = false;
          this.messageError = '';
          Object.values(error.error.errors).forEach(err=>{
            this.messageError += `${err} `;
          });
        }
      )
    }else{
      let identificacion = this.formGroup.controls['identificacion'].errors;
      let telefono = this.formGroup.controls['telefono'].errors;
      this.isverifyingForm = false;
      this.isErrorsForm = true;
      
      if(telefono || identificacion){
        this.messageError = 'Los campos "telefono | identificacion" debe ser numericos';
      }else{
        this.messageError = 'Todos los campos son obligatorios';
      }
    }
  }
}
