import { Component, EventEmitter, Input, OnInit, Output, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from 'src/app/core/services/driver.service';
import { ModalService } from 'src/app/modules/home/services/modal.service';

@Component({
  selector: 'app-modal-update',
  templateUrl: './modal-update.component.html',
  styleUrls: ['./modal-update.component.scss']
})
export class ModalUpdateComponent implements OnInit{
  
  public isOpenModalUpdate:boolean = false;
  public formGroup: FormGroup;
  public isErrorsForm: boolean = false;
  public messageError:string;
  public isverifyingForm:boolean = false;
  public driversData: any;

  @Output()
  freshDataTable:EventEmitter<any> = new EventEmitter<any>();
  
  constructor(
    public _modal:ModalService,
    public _driver:DriverService,
    public _formBuilder:FormBuilder
  ){}

  ngOnInit(): void {
    
    this._modal.dataClickRow.subscribe(
      (resp:any)=>{
        this.messageError = undefined;
        this.driversData = resp;
        this.formGroup = this._formBuilder.group({
          uuid: [resp.uuid,[Validators.required]],
          nombres: [resp.nombres,[Validators.required]],
          apellidos: [resp.apellidos,[Validators.required]],
          identificacion: [resp.identificacion,[Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
          direccion: [resp.direccion,[Validators.required]],
          telefono: [resp.telefono,[Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
          ciudad_nacimiento: [resp.ciudad_nacimiento,[Validators.required]]
        });
      }
    )
    
    this._modal.isOpenModalUpdate.subscribe(resp=>{
      this.isOpenModalUpdate = resp;
    });
  }

  closeModal(){
    this.isOpenModalUpdate = false;
  }

  send(){
    this.isverifyingForm = true;

    if(this.formGroup.valid){
      this._driver.update(this.formGroup.value).subscribe(
        (resp)=>{
          this.isverifyingForm = false;
          this.formGroup.reset();
          this.isOpenModalUpdate = false;
          this.freshDataTable.emit();
        },
        (error)=>{
          this.isErrorsForm = true;
          this.isverifyingForm = false;
          this.messageError = '';
          Object.values(error.error.errors).forEach(err=>{
            this.messageError += `${err} `;
          });
        }
      )
    }else{
      this.isverifyingForm = false;
      this.isErrorsForm = true;
      this.messageError = 'Todos los campos son obligatorios';
    }
  }

  delete(){
    this._driver.delete(this.formGroup.value).subscribe(resp=>{
          this.formGroup.reset();
          this.isOpenModalUpdate = false;
          this.freshDataTable.emit();
    });
  }
}
