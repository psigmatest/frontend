import { DriverService } from './../../../../core/services/driver.service';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import * as XLSX from 'xlsx';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit {

  public displayedColumns: string[] = ['nombres','apellidos','direccion','identificacion','telefono','ciudad_nacimiento'];
  public dataSource:any;
  
  @ViewChild(MatPaginator) 
  paginator: MatPaginator;

  @ViewChild('table') 
  table: ElementRef;

  constructor(
    public _modal:ModalService,
    public _drivers:DriverService
  ){}


  ngOnInit(): void {
    this.renderDrivers();
  }

  freshTable(){
    this.renderDrivers();
  }
  
  renderDrivers(){
    this._drivers.find().subscribe(
      (resp:any)=>{
        this.dataSource = new MatTableDataSource<any>(resp.data);
        this.dataSource.paginator = this.paginator;
      }
    );
  }
  exportAsExcel():void {
    let data = {...this.dataSource.data};

    Object.values(data).forEach((el:any)=>{
      delete el.uuid;
      delete el.vehicles;
    })
    
    const workSheet = XLSX.utils.json_to_sheet(Object.values(data));
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'conductores.xlsx');
  }

  openModalAdd(){
    this._modal.isOpenModalAdd.emit(true);
  }

  clickedRows(row:any){
    this._modal.dataClickRow.emit(row);
    this._modal.isOpenModalUpdate.emit(true);
  }
}

