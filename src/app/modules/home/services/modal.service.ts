import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  @Output('isOpenModalAdd')
  isOpenModalAdd:EventEmitter<boolean> = new EventEmitter<boolean>();
  
  @Output('isOpenModalUpdate')
  isOpenModalUpdate:EventEmitter<boolean> = new EventEmitter<boolean>();
  
  @Output('dataClickRow')
  dataClickRow:EventEmitter<any> = new EventEmitter<any>();

}
