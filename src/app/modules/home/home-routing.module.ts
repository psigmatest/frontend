import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DriversComponent } from './pages/drivers/drivers.component';
import { HomeComponent } from './home.component';
import { VehiclesComponent } from './pages/vehicles/vehicles.component';
import { AnalyticsComponent } from './pages/analytics/analytics.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent,
    children:[
      {
        path:'',
        component: AnalyticsComponent
      },
      {
        path:'drivers',
        component:DriversComponent
      },
      {
        path:'vehicles',
        component: VehiclesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
