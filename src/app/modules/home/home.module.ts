import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { DriversComponent } from './pages/drivers/drivers.component';
import { VehiclesComponent } from './pages/vehicles/vehicles.component';
import { HomeComponent } from './home.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { AnalyticsComponent } from './pages/analytics/analytics.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ModalAddComponent } from './pages/drivers/components/modal-add/modal-add.component';
import { ModalUpdateComponent } from './pages/drivers/components/modal-update/modal-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalAddVehiclesComponent } from './pages/vehicles/components/modal-add-vehicles/modal-add-vehicles.component';
import { ModalUpdateVehiclesComponent } from './pages/vehicles/components/modal-update-vehicles/modal-update-vehicles.component';

@NgModule({
  declarations: [
    DriversComponent,
    VehiclesComponent,
    HomeComponent,
    AnalyticsComponent,
    ModalAddComponent,
    ModalUpdateComponent,
    ModalAddVehiclesComponent,
    ModalUpdateVehiclesComponent,
    
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ComponentsModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class HomeModule { }
